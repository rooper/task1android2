package com.example.uuu9.task1;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.view.View.OnClickListener;
import android.widget.Toast;

import static com.example.uuu9.task1.R.id.btn2;


public class Fragment_A extends Fragment implements OnClickListener{
    EditText et;
    Button btn;
    View view;
    SharedPreferences sp;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_fragment_, container, false);
        btn = (Button) view.findViewById(R.id.btn2);
        et = (EditText) view.findViewById(R.id.et);
        sp = this.getActivity().getSharedPreferences("list",Context.MODE_PRIVATE);
        btn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        if(et.getText().toString().trim().length() == 0){
            Toast.makeText(view.getContext(),"Field is empty!",Toast.LENGTH_LONG);}

        else{

            Editor ed = sp.edit();
            int count = sp.getInt("count",0);
            ed.putString(count+"",et.getText().toString());
            ed.putInt("count",count+1);
            ed.commit();
            et.setText("");
        }
    }
}
