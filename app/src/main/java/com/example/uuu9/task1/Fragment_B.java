package com.example.uuu9.task1;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.view.View.OnClickListener;


public class Fragment_B extends Fragment implements OnClickListener{
    Button buttonRef;
    ListView lv;
    SharedPreferences spShow;
    String [] list;
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_fragment__b, container, false);
        buttonRef = (Button)view.findViewById(R.id.refresh);
        buttonRef.setOnClickListener(this);
        lv = (ListView)view.findViewById(R.id.listW);
        spShow = this.getActivity().getSharedPreferences("list",Context.MODE_PRIVATE);
        return view;
    }


    @Override
    public void onClick(View v) {
        list = new String[spShow.getInt("count",0)];
        for(int i = 0;i<spShow.getInt("count",0);i++){

            list[i]=spShow.getString(i+"",null);
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.listitem,list);
        lv.setAdapter(arrayAdapter);
    }
}
